# frozen_string_literal: true

class BitFlag
	INVALID_FLAG = 'Invalid flag'

	def self.from_array(flag_list, initial_value = 0)
		BitFlag.new(make_flags(flag_list), initial_value)
	end

	def self.make_flags(flag_list)
		hash =
			Array(flag_list)
			.each_with_index
			.to_h { |flag, index| [flag, 1 << index] }
		hash.default = 0
		hash
	end

	def initialize(flag_hash, initial_value = 0)
		@hash = flag_hash
		@value = initial_value.is_a?(Array) ? add(initial_value) : initial_value
	end

	def add(flags)
		@value = Array(flags).reduce(value_of) { |acc, flag| acc | @hash[flag] }
	end

	def add!(flags)
		flags = Array(flags)
		raise ArgumentError, INVALID_FLAG unless flags.all? { |flag| @hash.key?(flag) }

		add(flags)
	end

	def has?(flags)
		Array(flags)
			.map { |flag| @hash[flag] }
			.all? { |flag| bit?(@value, flag) }
	end

	def remove(flags)
		@value = Array(flags).reduce(@value) { |acc, flag| acc & ~@hash[flag] }
	end

	def remove!(flags)
		flags = Array(flags)
		raise ArgumentError, INVALID_FLAG unless flags.all? { |flag| @hash.key?(flag) }

		remove(flags)
	end

	def to_a
		@hash
			.filter { |_, value| bit?(value_of, value) }
			.keys
	end

	def to_h
		@hash.to_h { |key, value| [key, bit?(value_of, value)] }
	end

	def value_of
		@value || 0
	end

	private

	def bit?(value, bit)
		(value | bit) == value
	end
end
