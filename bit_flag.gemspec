# frozen_string_literal: true

Gem::Specification.new do |s|
	s.name         = 'bit_flag'
	s.version      = '0.2.0'
	s.summary      = 'Gem to create and manage bit flags.'

	s.description  = <<~EOF
		BitFlag provides an api to easily generate and manage bit flags.
  	EOF

  	s.files        = ['lib/bit_flag.rb']

  	s.homepage     = 'https://gitlab.com/SavagePixie/bit-flag'
  	s.authors      = ['SavagePixie']
  	s.email        = 'savagepixie@protonmail.com'
  	s.license      = 'MIT'

  	s.metadata     = {
	 	'bug_tracker_uri' => 'https://gitlab.com/SavagePixie/bit-flag/-/issues',
	 	'source_code_uri' => 'https://gitlab.com/SavagePixie/bit-flag'
  	}

  	s.cert_chain   = ['certs/bit_flag.pem']
  	s.signing_key  = File.expand_path('~/.ssh/gem-private_key.pem') if $0 =~ /gem\z/

  	s.add_development_dependency 'bundler', '~> 2.3'
  	s.add_development_dependency 'minitest', '~> 5.0'
end
