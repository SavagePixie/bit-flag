# BitFlag
BitFlag is a library to set up and handle bit flags for. This allows you to keep multiple boolean values in the same variable.

## Quick Example

```ruby
require 'bit_flag'

# Initialise the status value with the allowed flags
contact_preference = BitFlag.from_array(['call', 'email', 'sms'])
# Add one flag
contact_preference.add('call')
# Check if flag is on
puts contact_preference.has?('call')
# Get an array of all the flags that are on
puts contact_preference.to_a
```

## Class Methods

### from_array
```
BitFlag.from_array(flag_list, initial_value = 0) -> new_bit_flag
```

Returns a new `BitFlag` object and sets its flags using the given array. `initial_value` can be an integer or an array indicating which flags are on.

```ruby
preferences = ['call', 'email', 'sms']
# No initial_value sets all flags as off
contact = BitFlag.from_array(preferences)
# Flags can be initialised with integer
contact = BitFlag.from_array(preferences, 6)
# But it's easier to read with an array
contact = BitFlag.from_array(preferences, ['email', 'sms'])
```

### make_flags
```
BitFlag.make_flags(flag_list) -> new_flag_hash
```

Flags are internally stored as a hash, not a list. Sometimes, you might want to reuse the same list for multiple instances of the class. In that case, this method gives you a ready-to-use hash that you can pass to `BitFlag.new`.

```ruby
foods = BitFlag.make_flags(['dairy', 'meat', 'nuts', 'fish'])
allergies = BitFlag.new(foods)
food_preferences = BitFlag.new(foods)
```

### new
```
BitFlag.new(flag_hash, initial_value = 0) -> new_bit_flag
```

Returns a new `BitFlag` object and sets its flags using the given hash. Like [`from_array`](#from_array) its initial value can be set with an integer or an array.

```ruby
foods = BitFlag.make_flags(['dairy', 'meat', 'nuts', 'fish'])
allergies = BitFlag.new(foods)
food_preferences = BitFlag.new(foods)
```

## Instance Methods

### add
```
bit_flag.add(flags) -> integer
```

Sets a flag to true in `self` and returns the current flag value as an integer. It can take a single value or an array of them. If an invalid flag  is passed, it is ignored.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'])
contact.add('call') # Add one flag
contact.add(['email', 'sms']) # Add multiple flags at once
contact.add('invalid') # Nothing happens
```

### add!
```
bit_flag.add!(flags) -> integer
```

Same as [`add`](#add), but raises an `ArgumentError` if invalid flag is passed.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'])
contact.add!('sms') # Ok
contact.add!('invalid') # Raises error
contact.add!(['invalid', 'sms']) # Raises error
```

### has?
```
bit_flag.has?(flags) -> boolean
```

It can take a single flag or an array of them. Returns `true` if all given flags exists and is set to true in `self`, `false` otherwise.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['call', 'sms'])
contact.has?('call') == true
contact.has?(['call', 'sms']) == true
contact.has?(['call', 'email']) == false
contact.has?('invalid') == false
```

### remove
```
bit_flag.remove(flags) -> integer
```

Sets a flag to false in `self` and returns the current flag value as an integer. It can take a single value or an array of them. If an invalid flag  is passed, it is ignored.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['call', 'email', 'sms'])
contact.remove('call') # Remove one flag
contact.remove(['email', 'sms']) # Remove multiple flags at once
contact.remove('invalid') # Nothing happens
```

### remove!
```
bit_flag.remove!(flags) -> integer
```

Same as [`remove`](#remove), but raises an `ArgumentError` if invalid flag is passed.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['email', 'sms'])
contact.add!('sms') # Ok
contact.add!('invalid') # Raises error
contact.add!(['invalid', 'sms']) # Raises error
```

### to_a
```
bit_flag.to_a -> new_array
```

Returns a new array comprising all the flags that are `true` in `self`.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['email', 'sms'])
contact.to_a == ['email', 'sms']
```

### to_h
```
bit_flag.to_h -> new_hash
```

Returns a new hash representing the current state of `self`.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['email', 'sms'])
contact.to_h == {'call' => false, 'email' => true, 'sms' => true}
```

### value_of
```
bit_flag.value_of -> integer
```

Returns an integer representation of the flags  that are `true` in `self`.

```ruby
contact = BitFlag.from_array(['call', 'email', 'sms'], ['email', 'sms'])
contact.value_of == 6
```
