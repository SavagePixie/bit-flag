# frozen_string_literal: true

require 'minitest/autorun'
require 'bit_flag'

class BitFlagTest < Minitest::Test
	def setup
		@flags = BitFlag.make_flags(%w[a b c d])
		@bf = BitFlag.new(@flags)
	end

	def test_intiliase_with_value
		bf = BitFlag.new(@flags, 5)
		assert_equal 5, bf.value_of
	end

	def test_initialise_with_array
		bf = BitFlag.new(@flags, %w[b d])
		assert_equal 10, bf.value_of
	end

	def test_from_array
		bf = BitFlag.from_array(%w[a b c])
		assert_equal true, bf.instance_of?(BitFlag)
		assert_equal 0, bf.value_of
	end

	def test_from_array_initial_value
		bf = BitFlag.from_array(%w[a b c], 3)
		assert_equal 3, bf.value_of
	end

	def test_from_array_initial_array
		bf = BitFlag.from_array(%w[a b c], %w[a b])
		assert_equal 3, bf.value_of
	end

	def test_make_flags
		expected = {
			'a' => 1,
			'b' => 2,
			'c' => 4,
			'd' => 8,
			'e' => 16
		}

		assert_equal expected, BitFlag.make_flags(%w[a b c d e])
	end

	def test_add_flag
		assert_equal 2, @bf.add('b')
	end

	def test_add_existing_flag
		@bf.add('a')
		assert_equal 1, @bf.add('a')
	end

	def test_add_multiple_flags
		assert_equal 11, @bf.add(%w[a b d])
	end

	def test_add_undefined_flag
		assert_equal 0, @bf.add('e')
	end

	def test_add_undefined_flag_with_existing
		assert_equal 5, @bf.add(%w[a c e])
	end

	def test_add_bang
		assert_equal 3, @bf.add(%w[a b])
	end

	def test_add_bang_raises
		assert_raises(ArgumentError) { @bf.add!('e') }
	end

	def test_add_bang_raises_array
		assert_raises(ArgumentError) { @bf.add!(%w[a e]) }
	end

	def test_has
		@bf.add(%w[a b])
		assert_equal true, @bf.has?('a')
		assert_equal false, @bf.has?('c')
	end

	def test_has_multiple
		@bf.add(%w[a b])
		assert_equal true, @bf.has?(%w[a b])
	end

	def test_has_mixed
		@bf.add(%w[a b])
		assert_equal false, @bf.has?(%w[a b c])
	end

	def test_remove_flag
		@bf.add(%w[a b c])
		assert_equal 5, @bf.remove('b')
	end

	def test_remove_unexisting_flag
		assert_equal 0, @bf.remove('a')
	end

	def test_remove_multiple_flags
		@bf.add(%w[a b c d])
		assert_equal 10, @bf.remove(%w[a c])
	end

	def test_remove_undefined_flag
		@bf.add(['c'])
		assert_equal 4, @bf.remove('e')
	end

	def test_remove_undefined_flag_with_existing
		@bf.add(%w[a b])
		assert_equal 1, @bf.remove(%w[b c e])
	end

	def test_remove_bang
		@bf.add(%w[a b c])
		assert_equal 4, @bf.remove(%w[a b])
	end

	def test_remove_bang_raises
		assert_raises(ArgumentError) { @bf.remove!('e') }
	end

	def test_remove_bang_raises_array
		assert_raises(ArgumentError) { @bf.remove!(%w[a e]) }
	end

	def test_to_a
		@bf.add(%w[a b c])
		assert_equal %w[a b c], @bf.to_a
	end

	def test_to_a_empty
		assert_equal [], @bf.to_a
	end

	def test_to_h
		@bf.add(%w[a c])
		expected = {'a' => true, 'b' => false, 'c' => true, 'd' => false}
		assert_equal expected, @bf.to_h
	end

	def test_value_of
		@bf.add(%w[a c])
		assert_equal 5, @bf.value_of
	end
end
